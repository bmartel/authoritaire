<?php namespace Atrauzzi\Authoritaire\Model;

use Illuminate\Database\Eloquent\Model;


class Membership extends Model {

	protected $table = 'authoritaire_memberships';

	/**
	 * Membership <~~> Permission Relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function permissions() {

		return $this->morphToMany(
			'Atrauzzi\Authoritaire\Model\Permission',
			'permissionable',
			'authoritaire_permissionables'
		)->withTimestamps();
	}

	/**
	 * Membership <--> Role Relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function roles() {

		return $this->belongsToMany(
			'Atrauzzi\Authoritaire\Model\Role',
			'authoritaire_membership_roles',
			'membership_id'
		)->withTimestamps();
	}

	/**
	 * Membership ~~ Authorizable Relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
	 */
	public function authorizable() {

		return $this->morphTo();
	}

}