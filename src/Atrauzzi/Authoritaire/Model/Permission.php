<?php namespace Atrauzzi\Authoritaire\Model;

use Illuminate\Database\Eloquent\Model;


class Permission extends Model {

	protected $table = 'authoritaire_permissions';

	protected $fillable = [
		'name',
		'description'
	];

	/**
	 * Permission <~~> Membership Relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
	 */
	public function memberships() {

		return $this->morphedByMany(
			'Atrauzzi\Authoritaire\Model\Membership',
			'permissionable',
			'authoritaire_permissionables'
		)->withTimestamps();
	}

	/**
	 * Permission <~~> Role Relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function roles() {

		return $this->morphedByMany(
			'Atrauzzi\Authoritaire\Model\Role',
			'permissionable',
			'authoritaire_permissionables'
		)->withTimestamps();
	}

}