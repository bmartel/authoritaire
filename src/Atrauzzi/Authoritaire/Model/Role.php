<?php namespace Atrauzzi\Authoritaire\Model;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use \Exception;

class Role extends Model {

	protected $table = 'authoritaire_roles';

	protected $fillable = [
		'name',
		'description'
	];

	/**
	 * Retrieve All Authorizable Models which are members of this Role
	 *
	 * @return \Illuminate\Database\Eloquent\Collection
	 */
	public function authorizables() {

		$authorizables = new Collection();

		$this->memberships->each(function ($membership) use ($authorizables) {

			$authorizables[] = $membership->authorizable;
		});

		return $authorizables;
	}

	/**
	 * Add Authorizable Model to this Role
	 *
	 * @param Model $authorizable
	 * @throws \Exception
	 */
	public function addAuthorizable(Model $authorizable) {

		if ($authorizable instanceof Authorizable) {

			$membership = ($authorizable->membership) ? : $$authorizable->membership()->save(new Membership()); // Ensure authorizable has a membership

			$this->memberships()->save($membership);
		} else {
			throw new Exception(sprintf('The class %s does not use the AuthorizableImpl trait.', get_class($authorizable)));
		}

	}

	/**
	 * Role <~~> Permission Relation
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function permissions() {

		return $this->morphToMany(
			'Atrauzzi\Authoritaire\Model\Permission',
			'permissionable',
			'authoritaire_permissionables'
		)->withTimestamps();

	}

	/**
	 * Role <--> Membership
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
	 */
	public function memberships() {

		return $this->belongsToMany(
			'Atrauzzi\Authoritaire\Model\Membership',
			'authoritaire_membership_roles',
			'role_id'
		)->withTimestamps();
	}

}