<?php namespace Atrauzzi\Authoritaire\Model;


/**
 * Interface model's must implement to access the roles/permissions system
 *
 * Interface Authorizable
 *
 * @package Atrauzzi\Authoritaire\Model
 */
interface Authorizable {

	public function membership();

	public function roles();

	public function permissions();

	public function addRole(Role $role);

	public function is($checkRoles);

	public function can($checkPermissions);

}