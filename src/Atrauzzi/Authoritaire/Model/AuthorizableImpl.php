<?php namespace Atrauzzi\Authoritaire\Model;

use Illuminate\Support\Collection;

/**
 * Trait to include within the model implementing the Authorizable Interface
 *
 * Class AuthorizableImpl
 *
 * @package Atrauzzi\Authoritaire\Model
 */
trait AuthorizableImpl {

	/**
	 * Contracts Model to associate roles/permissions through a single Authorizable membership
	 *
	 * @return mixed
	 */
	public function membership() {

		return $this->morphOne('Atrauzzi\Authoritaire\Model\Membership', 'authorizable');
	}

	/**
	 * All roles for which this Model belongs to
	 *
	 * @return mixed
	 */
	public function roles() {

		return $this->membership->roles();
	}

	/**
	 * Add a Role to this Model
	 *
	 * @param Role $role
	 */
	public function addRole(Role $role) {

		$membership = ($this->membership) ? : $this->membership()->save(new Membership()); // Ensure authorizable has a membership

		$role->memberships()->save($membership);

	}

	/**
	 * Retrieves all permissions belonging to membership roles, and the roles assigned
	 *
	 * directly to the membership.
	 *
	 * @return Collection
	 */
	public function permissions() {

		$membershipPermissions = $this->membership->permissions;

		$rolePermissions = $this->roles;

		$permissionSet = new \Illuminate\Database\Eloquent\Collection();

		$permissionSet = $permissionSet->merge($membershipPermissions);

		foreach ($rolePermissions as $role) {
			$permissionSet = $permissionSet->merge($role->permissions);
		}

		return $permissionSet->unique();

	}

	/**
	 * Check if the Model belongs to the specified role
	 *
	 * @param $checkRoles
	 * @return bool
	 */
	public function is($checkRoles) {

		$checkRoles = is_array($checkRoles) ? $checkRoles : func_get_args();
		$roles = $this
			->roles()
			->lists('name');

		if (array_intersect($checkRoles, $roles) == $checkRoles) {
			return true;
		}

		return false;
	}

	/**
	 * Check if the Model is able to perform the action
	 *
	 * @param $checkPermissions
	 * @return bool
	 */
	public function can($checkPermissions) {

		$checkPermissions = is_array($checkPermissions) ? $checkPermissions : func_get_args();
		$permissions = $this
			->permissions()
			->lists('name');

		if (array_intersect($checkPermissions, $permissions) == $checkPermissions) {
			return true;
		}

		return false;
	}

}
