<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('authoritaire_memberships', function (Blueprint $table) {

			$table->increments('id')
				->unsigned();

			$table->integer('authorizable_id')
				->unsigned()
				->index();

			$table->string('authorizable_type');

			$table->timestamps();

			$table->unique([
				'authorizable_id',
				'authorizable_type'
			], 'authoritaire_memberships_unique');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('authoritaire_memberships');
	}

}
