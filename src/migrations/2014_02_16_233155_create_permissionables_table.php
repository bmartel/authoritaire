<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermissionablesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('authoritaire_permissionables', function (Blueprint $table) {

			$table->increments('id')->unsigned();

			$table->integer('permissionable_id')
				->unsigned()
				->index();

			$table->integer('permission_id')
				->unsigned()
				->index();

			$table->string('permissionable_type');

			$table->timestamps();

			$table->index([
				'permissionable_id',
				'permission_id',
				'permissionable_type'
			], 'authoritaire_permissionables_primary');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('authoritaire_permissionables');
	}

}
