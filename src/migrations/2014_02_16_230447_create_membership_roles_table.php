<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipRolesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {

		Schema::create('authoritaire_membership_roles', function (Blueprint $table) {

			$table->integer('membership_id')
				->unsigned()
				->index();

			$table->integer('role_id')
				->unsigned()
				->index();

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {

		Schema::drop('authoritaire_membership_roles');
	}

}
